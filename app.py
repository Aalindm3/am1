from flask import Flask

# Create an instance of the Flask application
app = Flask(__name__)

# Define a route for the root URL ('/')
@app.route('/')
def hello():
    return 'This is my Flask application using Gitlab CI in Python!!'

# Start the Flask application and make it listen on port 8080
if __name__ == '__main__':
    app.run(port=8080)
